# PEC1

Este juego se compone de tres escenas: el menú, el nivel y la pantalla de game over.
Para ello, se ha hecho uso de un script que controla el cambio entre escenas y se ha añadido a un gameObject que se ha dejado como Prefab para así poder usarlo en todas las escenas.


*  En la pantalla del menú se encuentran dos botones, uno para comenzar la partida y otro para salir del juego.

*  El juego se compone de un único nivel en el que tanto el jugador como el enemigo tienen que derrotarse mediante el uso de insultos. Cada personaje tiene tres vidas.
   Cada vez que un personaje vence, se muestra una animación en la que lucha con la espada. El resto del tiempo los personajes moverán su cuerpo pero manteniéndose en su sitio.
   Todo el control de animaciones se gestiona mediante los scripts AnimationPlayer y AnimationEnemy.
   Al principio de la partida, se escogerá de manera aleatoria quién comienza la partida, y a partir de ahí, se van rotando los turnos hasta que uno de los personajes gane.
   Para realizar el control del juego, el script GamePlayManager es el que controlará mediante distintos métodos, quién es el comienza la partida de forma aleatoria,
   además de ir gestionando la lista de preguntas y respuestas que se encuentran en el script QuestionsAnswers.

*  En la escena de Game Over, se mostrará la información del personaje que haya ganado la partida, si el jugador o el enemigo. Para poder getionar quién ha ganado, se hace uso del PlayerPrefbs.
   A su vez, se le da al jugador dos tipos de opciones, volver a jugar la partida o regresar al menú principal del juego.

En cada una de las escenas sonará una música distinta, la cual se gestionará mediante el script de SoundPlayer.
El vídeo que muestra el funcionamiento del juego se encuentra en la misma ruta que este fichero readme y se llama PEC1.mkv