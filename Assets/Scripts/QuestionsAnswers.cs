﻿public class QuestionsAnswers
{
    public string[] Questions= new[] {
        "¿Has dejado ya de usar pañales?",//P0
        "¡No hay palabras para describir lo asqueroso que eres!",//P1
        "¡He hablado con simios más educados que tu!",//P2
        "¡Llevarás mi espada como si fueras un pincho moruno!",//P3
        "¡Luchas como un ganadero!",//P4
        "¡No pienso aguantar tu insolencia aquí sentado!",//P5
        "¡Mi pañuelo limpiará tu sangre!",//P6
        "¡Ha llegado tu HORA, palurdo de ocho patas!",//P7
        "¡Una vez tuve un perro más listo que tu!",//P8
        "¡Nadie me ha sacado sangre jamás, y nadie lo hará!",//P9
        "¡Me das ganas de vomitar!",//P10
        "¡Tienes los modales de un mendigo!",//P11
        "¡He oído que eres un soplón despreciable!",//P12
        "¡La gente cae a mis pies al verme llegar!",//P13
        "¡Demasiado bobo para mi nivel de inteligencia!",//P14
        "Obtuve esta cicatriz en una batalla a muerte!"//P15
    };

    public string[] Answers= new[] {
        "Y yo tengo un SALUDO para ti, ¿Te enteras?",//P7
        "¿Por qué? ¿Acaso querías pedir uno prestado?",//P0
        "Me haces pensar que alguien ya lo ha hecho.",//P10
        "Sí que las hay, sólo que nunca las has aprendido.",//P1
        "Me alegra que asistieras a tu reunión familiar diaria.",//P2
        "Qué apropiado, tú peleas como una vaca.",//P4
        "Ya te están fastidiando otra vez las almorranas, ¿Eh?",//P5
        "Ah, ¿Ya has obtenido ese trabajo de barrendero?",//P6
        "Estaría acabado si la usases alguna vez.",//P14
        "Quería asegurarme de que estuvieras a gusto conmigo.",//P11
        "Espero que ya hayas aprendido a no tocarte la nariz.",//P15
        "Primero deberías dejar de usarla como un plumero.",//P3
        "Te habrá enseñado todo lo que sabes.",//P8
        "¿TAN rápido corres?",//P9
        "Qué pena me da que nadie haya oído hablar de ti",//P12
        "¿Incluso antes de que huelan tu aliento?"//P13
    };

    private bool boolCorrectAnswer = false;

    public bool CorrectAnswer(int indexQuestion, int indexAnswer)
    {
        boolCorrectAnswer = false;

        switch (indexQuestion)
        {
            case 0:
                if(indexAnswer==1)
                    boolCorrectAnswer = true;
                break;
            case 1:
                if (indexAnswer == 3)
                    boolCorrectAnswer = true;
                break;
            case 2:
                if (indexAnswer == 4)
                    boolCorrectAnswer = true;
                break;
            case 3:
                if (indexAnswer == 11)
                    boolCorrectAnswer = true;
                break;
            case 4:
                if (indexAnswer == 5)
                    boolCorrectAnswer = true;
                break;
            case 5:
                if (indexAnswer == 6)
                    boolCorrectAnswer = true;
                break;
            case 6:
                if (indexAnswer == 7)
                    boolCorrectAnswer = true;
                break;
            case 7:
                if (indexAnswer == 0)
                    boolCorrectAnswer = true;
                break;
            case 8:
                if (indexAnswer == 12)
                    boolCorrectAnswer = true;
                break;
            case 9:
                if (indexAnswer == 13)
                    boolCorrectAnswer = true;
                break;
            case 10:
                if (indexAnswer == 2)
                    boolCorrectAnswer = true;
                break;
            case 11:
                if (indexAnswer == 9)
                    boolCorrectAnswer = true;
                break;
            case 12:
                if (indexAnswer == 14)
                    boolCorrectAnswer = true;
                break;
            case 13:
                if (indexAnswer == 15)
                    boolCorrectAnswer = true;
                break;
            case 14:
                if (indexAnswer == 8)
                    boolCorrectAnswer = true;
                break;
            case 15:
                if (indexAnswer == 10)
                    boolCorrectAnswer = true;
                break;
            default:
                break;
        }

        return boolCorrectAnswer;
    }
}
