﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    //Variables
    public Text score;

    private void Start()
    {
        if(PlayerPrefs.GetInt("LifePlayer")==0)
        {
            score.text += "\n\n ENEMY";
        }
        else if (PlayerPrefs.GetInt("LifeEnemy") == 0)
        {
            score.text += "\n\n PLAYER";
        }
    }
}
