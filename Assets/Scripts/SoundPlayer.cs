﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public Sound sound;


    void Start()
    {
        SoundManager.Instance.PlayMusic(sound);
    }
}
