﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEnemy : MonoBehaviour
{
    private Rigidbody2D rigidbodyEnemy;
    private Animator animatorEnemy;

    void Start()
    {
        rigidbodyEnemy = GetComponent<Rigidbody2D>();
        animatorEnemy = GetComponent<Animator>();
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("EnemyAttack") == 1)
        {
            animatorEnemy.SetBool("EnemyAttack", true);
        }
        else
        {
            animatorEnemy.SetBool("EnemyAttack", false);
        }

        if(PlayerPrefs.GetInt("LifeEnemy") == 0)
        {
            animatorEnemy.SetInteger("LifeEnemy", 0);
        }
        else
        {
            animatorEnemy.SetInteger("LifeEnemy", PlayerPrefs.GetInt("LifeEnemy"));
        }
    }
}
