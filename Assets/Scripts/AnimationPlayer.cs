﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour
{
    private Rigidbody2D rigidbodyPlayer;
    private Animator animatorPlayer;

    void Start()
    {
        rigidbodyPlayer = GetComponent<Rigidbody2D>();
        animatorPlayer = GetComponent<Animator>();
    }
    
    void Update()
    {
        if(PlayerPrefs.GetInt("PlayerAttack")==1)
        {
            animatorPlayer.SetBool("PlayerAttack", true);
        }
        else
        {
            animatorPlayer.SetBool("PlayerAttack", false);
        }

        if (PlayerPrefs.GetInt("LifePlayer") == 0)
        {
            animatorPlayer.SetInteger("LifePlayer", 0);
        }
        else
        {
            animatorPlayer.SetInteger("LifePlayer", PlayerPrefs.GetInt("LifePlayer"));
        }
    }
}
