﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayManager : MonoBehaviour
{
    public Transform AnswersParent;
    public Button ButtonSelectionPrefab;
    public Text TextQuestion;
    public Text TextLifePlayer;
    public Text TextLifeEnemy;
    private bool isPlayer= false;
    private QuestionsAnswers questionAnswers = new QuestionsAnswers();
    private ManagerScene managerScene = new ManagerScene();
    private GameOver gameOver = new GameOver();
    private int indexQuestion;
    private int indexAnswer;
    private int lifePlayer = 3;
    private int lifeEnemy = 3;
    private int playerAttack = 0;
    private int enemyAttack = 0;

    private void Start()
    {
        PlayerPrefs.SetInt("PlayerAttack", 0);
        PlayerPrefs.SetInt("EnemyAttack", 0);
        PlayerPrefs.SetInt("LifePlayer", lifePlayer);
        PlayerPrefs.SetInt("LifeEnemy", lifeEnemy);
        RandomPlayer();
        StartRound();
    }

    private void Update()
    {
        TextLifePlayer.text= "Life Player: \n"+lifePlayer;
        TextLifeEnemy.text= "Life Player: \n"+ lifeEnemy;
    }

    private void RandomPlayer()
    {
        //Se comprueba quién comienza la partida
        var random = Random.Range(0, 2);
        
        if(random==0)
        {
            isPlayer = true;
        }
        else
        {
            isPlayer = false;
        }
    }

    private void StartRound()
    {
        //Se reinician los valores de los indices
        indexQuestion = 0;
        indexAnswer = 0;

        ClearUi();

        //Se comprueba quién comienza preguntando
        if (isPlayer)
        {
            //Si es el turno del jugador, se muestran para el jugador todas las preguntas disponibles
            FillUi(questionAnswers.Questions);
        }
        else
        {
            //Si es el turno del enemigo, se escoge una pregunta al azar
            var randomQuestion = Random.Range(0, questionAnswers.Questions.Length);
            TextQuestion.text = "Enemy attack:\n " + questionAnswers.Questions[randomQuestion];
            indexQuestion = randomQuestion;

            //Se muestran para el jugador todas las respuestas disponibles
            FillUi(questionAnswers.Answers);
        }
    }

    private void ClearUi()
    {
        playerAttack = 0;
        enemyAttack = 0;

        PlayerPrefs.SetInt("PlayerAttack", playerAttack);
        PlayerPrefs.SetInt("EnemyAttack", enemyAttack);

        foreach (Transform child in AnswersParent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void FillUi(string [] questionAnswer)
    {
        var y = 0;
        var index = 0;

        foreach (var question in questionAnswer)
        {
            var buttonSelectionCopy = Instantiate(ButtonSelectionPrefab, AnswersParent, true);
            buttonSelectionCopy.GetComponent<RectTransform>().localPosition = new Vector3(-18, y, 0);
            QuestionAnswerListener(buttonSelectionCopy.GetComponent<Button>(), index);
            buttonSelectionCopy.GetComponentInChildren<Text>().text = question;

            y -= 30;
            index++;
        }
    }

    private void QuestionAnswerListener(Button button, int index)
    {
        button.onClick.AddListener(() => {

            playerAttack = 0;
            enemyAttack = 0;

            if (isPlayer)
            {
                //Si es el turno del jugador, el index es de la pregunta
                indexQuestion = index;

                //El enemigo escoge una respuesta al azar
                var randomAnswer = Random.Range(0, questionAnswers.Answers.Length);
                TextQuestion.text = "Enemy respond:\n " + questionAnswers.Answers[randomAnswer];
                indexAnswer = randomAnswer;

                //Se comprueba si el enemigo ha acertado
                if(checkAnswer())
                {
                    enemyAttack = 1;
                    lifePlayer--;
                }
                else
                {
                    playerAttack = 1;
                    lifeEnemy--;
                }

                isPlayer = false;
            }
            else
            {
                //Si es el turno del enemigo, el index es de la respuesta
                indexAnswer = index;
                TextQuestion.text = "";

                //Se comprueba si el jugador ha acertado
                if (checkAnswer())
                {
                    playerAttack = 1;
                    lifeEnemy--;
                    
                }
                else
                {
                    enemyAttack = 1;
                    lifePlayer--;
                }

                isPlayer = true;
            }

            if (lifePlayer == 0 || lifeEnemy == 0)
            {
                Invoke("SceneGameOver", 3);
            }
            else
            {
                //Aquí se invocan las animaciones
                Invoke("StartRound",3);
            }

            PlayerPrefs.SetInt("PlayerAttack", playerAttack);
            PlayerPrefs.SetInt("EnemyAttack", enemyAttack);
            PlayerPrefs.SetInt("LifePlayer", lifePlayer);
            PlayerPrefs.SetInt("LifeEnemy", lifeEnemy);
        });
    }

    private bool checkAnswer()
    {
        if (questionAnswers.CorrectAnswer(indexQuestion, indexAnswer))
        {
            return true;
        }
            
        else
            return false;
    }

    private void SceneGameOver()
    {
        managerScene.ChangeScene("GameOver");
    }
}